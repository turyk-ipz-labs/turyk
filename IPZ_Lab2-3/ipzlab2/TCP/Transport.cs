namespace TCP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Transport")]
    public partial class Transport
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TransportId { get; set; }

        [StringLength(101)]
        public string TransportName { get; set; }

        [StringLength(101)]
        public string TransportLocation { get; set; }
    }
}

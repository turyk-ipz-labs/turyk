﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TCP;

namespace ipzlab2
{
    /// <summary>
    /// Логика взаимодействия для MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Window
    {
        private MainWindow MainW;
        public string curTransport;
        private bool active = false;
        private readonly TcpService tcpService;
        public MainMenu(MainWindow MainW)
        {
            this.MainW = MainW;
            tcpService = new TcpService();
            InitializeComponent();
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            SearchWindow sw = new SearchWindow(this);
            sw.Show();
        }

        private async void ShowRoute_Click(object sender, RoutedEventArgs e)
        {
            RouteWindow rw = new RouteWindow(this);
            rw.textBlock.Text = this.curTransport;

            try
            {
                if (this.curTransport == null)
                {
                    throw new ArgumentException("Не вибраний транспорт");
                }
                string request = " ";
                request = tcpService.SerializeSearchStops(this.curTransport);
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
                List<string> stops = tcpService.DeserializeSearchStops(response);

                if (stops.Count == 0)
                {
                    throw new ArgumentException("Даний транспорт немає зупинок");
                }
                else
                {
                    rw.listBox.ItemsSource = stops;
                }
                rw.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async void Show_Click(object sender, RoutedEventArgs e)
        {
            RouteWindow rw = new RouteWindow(this);
            rw.textBlock.Text = this.curTransport;

            try
            {
                if (this.curTransport == null)
                {
                    throw new ArgumentException("Не вибраний транспорт");
                }
                string request = " ";
                request = tcpService.SerializeShowPos(this.curTransport);
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
                List<string> stops = tcpService.DeserializeShowPos(response);

               
                if (stops.Count == 0)
                {
                    throw new ArgumentException("Даний транспорт немає зупинок");
                }
                else
                {
                    rw.textBlockPos.Text = "Транспорт зараз на зупинці: " + stops.Last();
                    stops.RemoveAt(stops.Count() - 1);
                    rw.listBox.ItemsSource = stops;
                }
                rw.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            MainW.Back(false);
            this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            MainW.Back(true);
        }

        public async void Back(bool mode)
        {
            if (mode == true && active == false)
            {
                string request = "LogOut";
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                Application.Current.Shutdown();
            }
            else if (mode == true && active == true)
            {
                this.Show();
                active = false;
            }
            else if (mode == false)
            {
                active = true;
            }
        }
    }
}

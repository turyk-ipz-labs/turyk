﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TCP;

namespace ipzlab2
{
    /// <summary>
    /// Логика взаимодействия для SearchWindow.xaml
    /// </summary>
    public partial class SearchWindow : Window
    {
        private MainMenu mn;
        private readonly TcpService tcpService;
        public SearchWindow(MainMenu mn)
        {
            this.mn = mn;
            InitializeComponent();
            tcpService = new TcpService();
        }

        private async void Search_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string request = " ";
                MainWindow.inputNotNull(searchBox.Text);
                if (comboBox.SelectedIndex == -1)
                {
                    throw new ArgumentException("Не вибраний тип пошуку");
                }
                if (comboBox.SelectedIndex == 0)
                {
                    request = tcpService.SerializeSearchByNumber(int.Parse(searchBox.Text));
                }
                if (comboBox.SelectedIndex == 1)
                {
                    request = tcpService.SerializeSearchByStop(searchBox.Text);

                }

                
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
                List<string> stops = tcpService.DeserializeSearch(response);

                if (stops.Count == 0 && comboBox.SelectedIndex == 0)
                {
                    throw new ArgumentException("Такого транспорту немає");
                }
                else if(stops.Count == 0 && comboBox.SelectedIndex == 1)
                {
                    throw new ArgumentException("Такої зупинки немає");
                }
                else
                {
                    listBox.ItemsSource = stops;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Select_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (listBox.SelectedIndex == -1)
                {
                    throw new ArgumentException("Не вибраний транспорт");
                }
                mn.curTransport = listBox.SelectedItem.ToString();
                mn.numberTransport.Text = "Вибраний транспорт: " + listBox.SelectedItem;
                mn.numberTransport.Text = mn.numberTransport.Text.Replace("System.Windows.Controls.ListBoxItem: ", "");
                mn.Back(false);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            mn.Back(false);
            this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            mn.Back(true);
        }
    }
}

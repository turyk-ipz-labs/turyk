﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TCP
{
    class Program
    {
        static TcpListener listener;
        static void Main(string[] args)
        {
          ///  Console.OutputEncoding = Encoding.Unicode;
           /// Console.InputEncoding = Encoding.Unicode;
            using (DataBase context = new DataBase())
            {
                int port = 5000;
                string address = "127.0.0.1";
                try
                {
                    listener = new TcpListener(IPAddress.Parse(address), port);
                    listener.Start();
                    Console.WriteLine("Очiкування пiдключення ...");

                    while (true)
                    {
                        TcpClient client = listener.AcceptTcpClient();
                        ClientObject clientObject = new ClientObject(client, context);
                        Thread clientThread = new Thread(new ThreadStart(clientObject.Process));
                        clientThread.Start();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    if (listener != null)
                    {
                        listener.Stop();
                    }
                }
            }
        }
    }
}
